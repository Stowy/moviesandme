# Getting started

You have to get a token from [TMDB](https://www.themoviedb.org) and put it in `API/token.js` like this:

```Javascript
export default API_TOKEN = "your_token";
```

Then you need to run `npm install` to install the dependencies.
