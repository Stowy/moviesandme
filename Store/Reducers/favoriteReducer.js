const initialState = { favoriteMovies: [] }

export default function toggleFavorite(state = initialState, action) {
    let nextState;
    switch (action.type) {
        case 'TOGGLE_FAVORITE':
            const favoriteMovieIndex = state.favoriteMovies.findIndex(item => item.id === action.value.id);
            if(favoriteMovieIndex !== -1) {
                // The movie is allready in our favorites, we delete it from our list
                nextState = {
                    ...state,
                    favoriteMovies: state.favoriteMovies.filter( (item, index) => index !== favoriteMovieIndex),
                }
            }
            else {
                // The movie is not in our favorites, we add it to our list
                nextState = {
                    ...state,
                    favoriteMovies: [...state.favoriteMovies, action.value],
                }
            }
            return nextState || state;
        default:
            return state;
    }
}