import { Platform } from 'react-native';

// Display ios style if we're on an apple product, material style otherwise
const ionprefix = Platform.OS === 'ios' || Platform.OS === 'macos' ? 'ios-' : 'md-';

export default ionprefix;