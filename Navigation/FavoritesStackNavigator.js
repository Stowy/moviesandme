import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Favorites from "../Components/Favorites";
import MovieDetail from '../Components/MovieDetail';

const FavoritesStack = createStackNavigator();

export default class FavoritesStackNavigator extends React.Component {
    render() {
        return (
            <FavoritesStack.Navigator>
                <FavoritesStack.Screen name="Favorites" component={Favorites} options={{ title: 'Favoris' }} />
                <FavoritesStack.Screen name="MovieDetail" component={MovieDetail} options={{ title: 'Détail du film' }} />
            </FavoritesStack.Navigator>
        );
    }
}
