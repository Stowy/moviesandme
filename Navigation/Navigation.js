import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import SearchStackNavigator from './SearchStackNavigator';
import FavoritesStackNavigator from './FavoritesStackNavigator';
import Tests from "../Components/Tests";
import ionprefix from "../Helpers/ionprefix";

const Tab = createBottomTabNavigator();

export default class Navigation extends React.Component {
    render() {
        return (
            <NavigationContainer>
                <Tab.Navigator
                    screenOptions={({ route }) => ({
                        tabBarIcon: ({ focused, color, size }) => {
                            let iconName = ionprefix;

                            // Display the good icon for the good route
                            switch (route.name) {
                                case 'Search':
                                    iconName += 'search';
                                    break;
                                case 'Favorites':
                                    iconName += 'heart';
                                    break;
                                case 'Tests':
                                    iconName += 'construct';
                                    break;
                                default:
                                    return;
                            }

                            return <Ionicons name={iconName} size={size} color={color} />
                        }
                    })}>
                    {/* <Tab.Screen name="Tests" component={Tests} /> */}
                    <Tab.Screen name="Search" component={SearchStackNavigator} />
                    <Tab.Screen name="Favorites" component={FavoritesStackNavigator} />
                </Tab.Navigator>
            </NavigationContainer>
        );
    }
}
