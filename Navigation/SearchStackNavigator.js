import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Search from "../Components/Search";
import MovieDetail from '../Components/MovieDetail';

const SearchStack = createStackNavigator();

export default class SearchStackNavigator extends React.Component {
    render() {
        return (
            <SearchStack.Navigator>
                <SearchStack.Screen name="Search" component={Search} options={{ title: 'Recherche' }} />
                <SearchStack.Screen name="MovieDetail" component={MovieDetail} options={{ title: 'Détail du film' }} />
            </SearchStack.Navigator>
        );
    }
}
