import React from 'react';
import { Animated } from 'react-native';

class EnlargeShrink extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            viewScale: new Animated.Value(this._getScale()),
        }
    }

    _getScale() {
        let scale = 1;
        if (this.props.shouldEnlarge) {
            scale = 2;
        }

        return scale;
    }

    componentDidUpdate() {
        Animated.spring(this.state.viewScale, {
            toValue: this._getScale(),
            speed: 2,
            useNativeDriver: true,
        }).start();
    }

    render() {
        return (
            <Animated.View
                style={{
                    transform: [{ scale: this.state.viewScale }]
                }}>
                {this.props.children}
            </Animated.View>
        );
    }
}

export default EnlargeShrink;