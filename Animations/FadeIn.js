import React from 'react';
import { Animated, Dimensions } from "react-native";

class FadeIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posX: new Animated.Value(Dimensions.get('window').width),
        }
    }

    componentDidMount() {
        Animated.spring(this.state.posX, {
            toValue: 0,
            useNativeDriver: true,
        }).start();
    }

    render() {
        return (
            <Animated.View
                style={{
                    transform: [{ translateX: this.state.posX }]
                }}>
                {this.props.children}
            </Animated.View>
        );
    }
}

export default FadeIn;