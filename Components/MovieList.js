import React from 'react';
import { StyleSheet, FlatList } from 'react-native';
import MovieItem from "./MovieItem";
import { connect } from "react-redux";

class MovieList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: [],
        }
    }

    _displayDetailForMovie = (idMovie) => {
        this.props.navigation.navigate("MovieDetail", { idMovie: idMovie });
    }

    render() {
        return (

            <FlatList
                style={styles.list}
                data={this.props.movies}
                extraData={this.props.favoriteMovies}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({ item }) =>
                    <MovieItem
                        movie={item}
                        displayDetailForMovie={this._displayDetailForMovie}
                        isFavorite={(this.props.favoriteMovies.findIndex(movie => movie.id === item.id) !== -1)}
                    />}
                onEndReachedThreshold={0.5}
                onEndReached={() => {
                    if (!this.props.favoriteList && this.props.page < this.props.totalPages) {
                        this.props.loadMovies();
                    }
                }}
            />
        );
    }
}

const styles = StyleSheet.create({
    list: {
        flex: 1
    }
});

const mapStateToProps = state => {
    return {
        favoriteMovies: state.favoriteMovies,
    }
}

export default connect(mapStateToProps)(MovieList);