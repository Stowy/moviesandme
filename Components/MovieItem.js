import React from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { getImageFromApi } from "../API/TMDBApi";
import FadeIn from '../Animations/FadeIn';

export default class MovieItem extends React.Component {
    _displayFavoriteImage() {
        if (this.props.isFavorite) {
            return (
                <Image
                    style={styles.favorite_image}
                    source={require('../Images/ic_favorite.png')}
                />
            );
        }
    }

    render() {
        const { movie, displayDetailForMovie } = this.props;
        return (
            <FadeIn>
                <TouchableOpacity
                    style={styles.main_container}
                    onPress={() => displayDetailForMovie(movie.id)}>
                    <Image
                        style={styles.movie_poster}
                        source={{ uri: getImageFromApi(movie.poster_path) }}
                    />
                    <View style={styles.content}>
                        <View style={styles.header}>
                            {this._displayFavoriteImage()}
                            <Text style={styles.title_text}>{movie.title}</Text>
                            <Text style={styles.vote_text}>{movie.vote_average}</Text>
                        </View>
                        <View style={styles.description_container}>
                            <Text
                                style={styles.description_text}
                                numberOfLines={6}>
                                {movie.overview}
                            </Text>
                        </View>
                        <View style={styles.date_container}>
                            <Text style={styles.release_date}>Sorti le {movie.release_date}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </FadeIn>

        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        height: 190,
        flexDirection: 'row',
    },
    content: {
        flexDirection: 'column',
        flex: 1,
        margin: 5,
    },
    header: {
        flexDirection: 'row',
        flex: 3,
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 5,
    },
    vote_text: {
        fontWeight: 'bold',
        fontSize: 26,
        color: '#666666',
    },
    movie_poster: {
        width: 120,
        height: 180,
        margin: 5,
    },
    description_text: {
        color: '#666666',
        fontStyle: 'italic',
    },
    release_date: {
        textAlign: 'right',
        fontSize: 14,
    },
    description_container: {
        flex: 7,
    },
    date_container: {
        flex: 1,
    },
    favorite_image: {
        width: 25,
        height: 25,
        marginRight: 5,
    }
})