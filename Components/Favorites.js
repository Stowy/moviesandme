import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from "react-redux";
import MovieList from "../Components/MovieList";

class Favorites extends React.Component {
    render() {
        return (
            <MovieList
                movies={this.props.favoriteMovies}
                navigation={this.props.navigation}
                favoriteList={true}
            />
        );
    }
}

const styles = StyleSheet.create({
});

const mapStateToProps = state => {
    return {
        favoriteMovies: state.favoriteMovies,
    }
}

export default connect(mapStateToProps)(Favorites);