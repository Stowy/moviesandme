import React from 'react';
import { StyleSheet, View, Text, ActivityIndicator, ScrollView, Image, TouchableOpacity, Share, Platform } from 'react-native';
import { getMovieDetail, getImageFromApi } from '../API/TMDBApi';
import moment from 'moment';
import numeral from 'numeral';
import { connect } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import EnlargeShrink from "../Animations/EnlargeShrink";

class MovieDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            movie: undefined,
            isLoading: false,
        }

        this._shareMovie = this._shareMovie.bind(this);
    }

    _setHeaderRight = state => {
        if (state.movie != undefined && Platform.OS == 'ios') {
            return (
                <TouchableOpacity style={styles.share_touchable_headerrightbutton} onPress={() => state.shareMovie()}>
                    <Ionicons name="ios-share" style={styles.share_icon} />
                </TouchableOpacity>
            );
        }
    }

    _updateNavigationParams() {
        this.props.navigation.setOptions({
            headerRight: () => this._setHeaderRight({ movie: this.state.movie, shareMovie: this._shareMovie }),
        });
    }

    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator color='#999999' size='large' />
                </View>
            );
        }
    }

    _displayFavoriteImage() {
        let sourceImage = require('../Images/ic_favorite_border.png');
        let shouldEnlarge = false;
        if (this.props.favoriteMovies.findIndex(item => item.id === this.state.movie.id) !== -1) {
            sourceImage = require('../Images/ic_favorite.png');
            shouldEnlarge = true;
        }
        return (
            <EnlargeShrink shouldEnlarge={shouldEnlarge}>
                <Image
                    style={[styles.favorite_image, {}]}
                    source={sourceImage} />
            </EnlargeShrink>

        );
    }

    _toggleFavorite() {
        const action = { type: "TOGGLE_FAVORITE", value: this.state.movie }
        this.props.dispatch(action);
    }

    _displayMovie() {
        const { movie } = this.state;
        if (movie != undefined) {
            return (
                <ScrollView style={styles.scrollview_container}>
                    <Image
                        source={{ uri: getImageFromApi(movie.backdrop_path) }}
                        style={styles.backdrop} />
                    <Text style={styles.title}>{movie.title}</Text>
                    <TouchableOpacity
                        style={styles.favorite_container}
                        onPress={() => this._toggleFavorite()}>
                        {this._displayFavoriteImage()}
                    </TouchableOpacity>
                    <Text style={styles.overview}>{movie.overview}</Text>
                    <Text style={styles.info}>Sorti le {moment(new Date(movie.release_date)).format('DD/MM/YYYY')}</Text>
                    <Text style={styles.info}>Note : {movie.vote_average} / 10</Text>
                    <Text style={styles.info}>Nombre de votes : {movie.vote_count}</Text>
                    <Text style={styles.info}>Budget : {numeral(movie.budget).format('0,0[.]00 $')}</Text>
                    <Text style={styles.info}>
                        Genre(s) : {movie.genres.map((genre) => genre.name).join(" / ")}</Text>
                    <Text style={styles.info}>Companie(s) : {movie.production_companies.map((companie) => companie.name).join(" / ")}</Text>
                </ScrollView>
            );
        }
    }

    _shareMovie() {
        const { movie } = this.state;
        const message = `${movie.title}\n${movie.overview}`;
        Share.share({
            title: movie.title,
            message: message
        });
    }

    _displayFloatingActionButton() {
        const { movie } = this.state;
        if (movie != undefined && Platform.OS === 'android') {
            return (
                <TouchableOpacity
                    style={styles.share_touchable_floatingactionbutton}
                    onPress={() => this._shareMovie()}>
                    <Ionicons name="md-share" style={styles.share_icon} />
                </TouchableOpacity>
            );
        }
    }

    componentDidMount() {
        const favoriteMovieIndex = this.props.favoriteMovies.findIndex(item => item.id === this.props.route.params.idMovie);
        if (favoriteMovieIndex !== -1) {
            // The movie is allready in our favorites
            this.setState({
                movie: this.props.favoriteMovies[favoriteMovieIndex],
                isLoading: false,
            }, () => this._updateNavigationParams());
            return;
        }
        // The movie is not in our favorites, we have to do an API call
        this.setState({ isLoading: true });
        getMovieDetail(this.props.route.params.idMovie).then(data => {
            this.setState({
                movie: data,
                isLoading: false,
            }, () => this._updateNavigationParams());
        });
    }

    render() {
        return (
            <View style={styles.main_container}>
                {this._displayLoading()}
                {this._displayMovie()}
                {this._displayFloatingActionButton()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    scrollview_container: {
        flex: 1,
    },
    backdrop: {
        height: 169,
        margin: 5,
    },
    title: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 24,
        margin: 5
    },
    overview: {
        fontStyle: 'italic',
        color: 'gray',
        margin: 5,
    },
    info: {
        marginLeft: 5,
    },
    favorite_container: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 80,
    },
    favorite_image: {
        height: 40,
        width: 40,
    },
    share_touchable_floatingactionbutton: {
        position: 'absolute',
        width: 60,
        height: 60,
        right: 30,
        bottom: 30,
        borderRadius: 30,
        backgroundColor: '#e91e63',
        justifyContent: 'center',
        alignItems: 'center'
    },
    share_icon: {
        fontSize: 24
    },
    share_touchable_headerrightbutton: {
        marginRight: 8,
    },
});

const mapStateToProps = (state) => {
    // We only map the informations we want
    // In this case: the favorite movies
    return {
        favoriteMovies: state.favoriteMovies,
    };
}

export default connect(mapStateToProps)(MovieDetail);